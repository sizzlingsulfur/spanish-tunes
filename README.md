# spanish-tunes

tunes of a spanish nature.

## albums

- [LadoB][1] Bocafloja
- [[R]][2] Residente
- [A Titulo personal][3] Bocafloja
- [Un Dia En Suburbia][4] Nach
- [Progreso Hip Hop][5] Sector Lucido
- [Melokarma][6] Akil Ammar
- [Los De Atras Vienen Conmigo][7] Calle 13
- [Dignidad Rebelde][10] Belona MC
- [Coniencia Social][12] Guerrilla Okulto
- [Poblacional][13] Salvaje Decibel

## tracks

- [Accion Intransigente][8] Hache ST
- [Campesino][9] Rebel Diaz ft Bocafloja
- [Aperturas][11] Belona Mc
- [Ralla][14] Salvaje Decibel
- [Rap Bruto][15] Residente ft Nach
- [Guerra][16] Residente
- [No Hay Naide Como Tu][17] Calle 13
- [Mi Barro][18] Akil Ammar
- [Sicario][19] Dilinyer
- [La Glock][20] Flow Mafia

[1]: https://www.youtube.com/watch?v=zdpgg8DzrKY&t=835s
[2]: https://www.youtube.com/watch?v=qBgyJBYJ7Lk&t=2190s
[3]: https://www.youtube.com/watch?v=aNdUFbKwpZg
[4]: https://www.youtube.com/watch?v=iNmb5CFllr0
[5]: https://www.youtube.com/watch?v=rdrKhOtZ6pA
[6]: https://www.youtube.com/watch?v=3hSf_qrmmEM
[7]: https://www.youtube.com/watch?v=RN7IKnPyjCg&t=370s
[8]: https://www.youtube.com/watch?v=ssqc4_jkDjY
[9]: https://www.youtube.com/watch?v=tQCxM_GTe80
[10]: https://www.youtube.com/watch?v=7NfRw5MA_Ts
[11]: https://www.youtube.com/watch?v=dG1Qbv-c-SI
[12]: https://www.youtube.com/watch?v=iBRtdgzE4lc
[13]: https://www.youtube.com/watch?v=XImQR_S0KgM
[14]: https://www.youtube.com/watch?v=FtwuNqZZU1w
[15]: https://www.youtube.com/watch?v=vFAOKWaEctg
[16]: https://www.youtube.com/watch?v=Zl_GlPquElI
[17]: https://www.youtube.com/watch?v=t0DeJ5HeG8o
[18]: https://www.youtube.com/watch?v=kA2-A4PTaV8
[19]: https://www.youtube.com/watch?v=FuBziWiIyXU
[20]: https://www.youtube.com/watch?v=q2Ua1ccIXQo